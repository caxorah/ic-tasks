package com.verifone.icTasks.entity;
import com.verifone.icTasks.service.PeriodService;


public class TaskDao {

    private int id;
    //need period as a String instead of int for  better user experience
    private String period;
    private String entityOut;
    private String entityIn;
    private String currency;
    private String name;
    private String transactionType;
    private String dataProvider;
    private String description;
    private String status;
    private String comment;
    private int templateId;
    private Integer batchNumber;

    public Task convertToTask(){
        Task task = new Task();
        task.setId(this.getId());
        task.setTemplateId(this.getTemplateId());
        task.setDescription(this.getDescription());
        task.setName(this.getName());
        task.setCurrency(this.getCurrency());
        task.setTransactionType(this.getTransactionType());
        task.setDataProvider(this.getDataProvider());
        task.setComment(this.getComment());
        task.setEntityIn(this.getEntityIn());
        task.setEntityOut(this.getEntityOut());
        task.setStatus(this.getStatus());
        task.setPeriod(PeriodService.parseStringPeriodToInt(this.period));
        task.setBatchNumber(this.getBatchNumber());
        return task;
    }

    public TaskDao() {
    }

    public TaskDao fromTask(Task task){
        this.setComment(task.getComment());
        this.setCurrency(task.getCurrency());
        this.setDataProvider(task.getDataProvider());
        this.setDescription(task.getDescription());
        this.setId(task.getId());
        this.setEntityIn(task.getEntityIn());
        this.setEntityOut(task.getEntityOut());
        this.setName(task.getName());
        this.setStatus(task.getStatus());
        this.setTemplateId(task.getTemplateId());
        this.setTransactionType(task.getTransactionType());
        this.setPeriod(PeriodService.parseIntPeriodToString(task.getPeriod()));
        this.setBatchNumber(task.getBatchNumber());
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getEntityOut() {
        return entityOut;
    }

    public void setEntityOut(String entityOut) {
        this.entityOut = entityOut;
    }

    public String getEntityIn() {
        return entityIn;
    }

    public void setEntityIn(String entityIn) {
        this.entityIn = entityIn;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(String dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    @Override
    public String toString() {
        return "TaskDao{" +
                "id=" + id +
                ", period='" + period + '\'' +
                ", entityOut='" + entityOut + '\'' +
                ", entityIn='" + entityIn + '\'' +
                ", currency='" + currency + '\'' +
                ", name='" + name + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", dataProvider='" + dataProvider + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", comment='" + comment + '\'' +
                ", templateId=" + templateId +
                '}';
    }
}
