package com.verifone.icTasks.service;

import com.verifone.icTasks.entity.TaskTemplate;
import com.verifone.icTasks.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TemplateService {

    @Autowired
    TemplateRepository repository;

    public void saveTemplate(TaskTemplate template){
        repository.addOrEditTemplate(template);
    }

    public List<TaskTemplate> getTemplates() {
        return repository.getAllTemplates();
    }

    public TaskTemplate getTemplate(String id_str){
        int id = Integer.parseInt(id_str);
        return repository.getTemplateById(id);
    }

    public List<TaskTemplate> getTemplatesForPeriod(int period) {
        return repository.getTemplatesByPeriod(period);
    }


}
