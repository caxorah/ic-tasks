package com.verifone.icTasks.entity;

import javax.persistence.*;

@Entity
@Table(name = "templates")
public class TaskTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "entity_out")
    private String entityOut;

    @Column(name = "entity_in")
    private String entityIn;

    @Column(name = "currency")
    private String currency;

    @Column(name = "name")
    private String name;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "data_provider")
    private String dataProvider;

    @Column(name = "description")
    private String description;

    @Column(name = "is_active")
    private boolean active;

    @Column(name = "jan")
    private boolean jan;

    @Column(name = "feb")
    private boolean feb;

    @Column(name = "mar")
    private boolean mar;

    @Column(name = "apr")
    private boolean apr;

    @Column(name = "may")
    private boolean may;

    @Column(name = "jun")
    private boolean jun;

    @Column(name = "jul")
    private boolean jul;

    @Column(name = "aug")
    private boolean aug;

    @Column(name = "sep")
    private boolean sep;

    @Column(name = "oct")
    private boolean oct;

    @Column(name = "nov")
    private boolean nov;

    @Column(name = "dec")
    private boolean dec;

    public TaskTemplate() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEntityOut() {
        return entityOut;
    }

    public void setEntityOut(String entityOut) {
        this.entityOut = entityOut;
    }

    public String getEntityIn() {
        return entityIn;
    }

    public void setEntityIn(String entityIn) {
        this.entityIn = entityIn;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(String dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isJan() {
        return jan;
    }

    public void setJan(boolean jan) {
        this.jan = jan;
    }

    public boolean isFeb() {
        return feb;
    }

    public void setFeb(boolean feb) {
        this.feb = feb;
    }

    public boolean isMar() {
        return mar;
    }

    public void setMar(boolean mar) {
        this.mar = mar;
    }

    public boolean isApr() {
        return apr;
    }

    public void setApr(boolean apr) {
        this.apr = apr;
    }

    public boolean isMay() {
        return may;
    }

    public void setMay(boolean may) {
        this.may = may;
    }

    public boolean isJun() {
        return jun;
    }

    public void setJun(boolean jun) {
        this.jun = jun;
    }

    public boolean isJul() {
        return jul;
    }

    public void setJul(boolean jul) {
        this.jul = jul;
    }

    public boolean isAug() {
        return aug;
    }

    public void setAug(boolean aug) {
        this.aug = aug;
    }

    public boolean isSep() {
        return sep;
    }

    public void setSep(boolean sep) {
        this.sep = sep;
    }

    public boolean isOct() {
        return oct;
    }

    public void setOct(boolean oct) {
        this.oct = oct;
    }

    public boolean isNov() {
        return nov;
    }

    public void setNov(boolean nov) {
        this.nov = nov;
    }

    public boolean isDec() {
        return dec;
    }

    public void setDec(boolean dec) {
        this.dec = dec;
    }

    @Override
    public String toString() {
        return "TaskTemplate{" +
                "id=" + id +
                ", entityOut='" + entityOut + '\'' +
                ", entityIn='" + entityIn + '\'' +
                ", currency='" + currency + '\'' +
                ", name='" + name + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", dataProvider='" + dataProvider + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
