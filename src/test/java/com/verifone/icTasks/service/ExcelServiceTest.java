package com.verifone.icTasks.service;

import com.verifone.icTasks.entity.Task;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class ExcelServiceTest {

    @Test
    void findBatchInTracker() throws IOException {

        Task task = new Task();
        task.setEntityOut("361");
        task.setName("IC billing to EN110");
        task.setPeriod(201911);
        int expectedBatch = 13823;

        ExcelService excelService = new ExcelService();
        int found = excelService.findBatchInTracker(task);

        assertEquals(expectedBatch,found);

    }
}