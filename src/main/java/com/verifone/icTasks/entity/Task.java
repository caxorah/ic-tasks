package com.verifone.icTasks.entity;

import javax.persistence.*;

@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "period")
    private int period;

    @Column(name = "entity_out")
    private String entityOut;

    @Column(name = "entity_in")
    private String entityIn;

    @Column(name = "currency")
    private String currency;

    @Column(name = "name")
    private String name;

    @Column(name = "transaction_type")
    private String transactionType;

    @Column(name = "data_provider")
    private String dataProvider;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    private String status;

    @Column(name = "comment")
    private String comment;

    @Column(name = "template_id")
    private int templateId;

    @Column(name = "batch_number")
    private Integer batchNumber;

    public Task() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEntityOut() {
        return entityOut;
    }

    public void setEntityOut(String entityOut) {
        this.entityOut = entityOut;
    }

    public String getEntityIn() {
        return entityIn;
    }

    public void setEntityIn(String entityIn) {
        this.entityIn = entityIn;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDataProvider() {
        return dataProvider;
    }

    public void setDataProvider(String dataProvider) {
        this.dataProvider = dataProvider;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", period=" + period +
                ", entityOut='" + entityOut + '\'' +
                ", entityIn='" + entityIn + '\'' +
                ", currency='" + currency + '\'' +
                ", name='" + name + '\'' +
                ", transactionType='" + transactionType + '\'' +
                ", dataProvider='" + dataProvider + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
