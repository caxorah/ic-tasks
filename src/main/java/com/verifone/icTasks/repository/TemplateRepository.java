package com.verifone.icTasks.repository;

import com.verifone.icTasks.entity.TaskTemplate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TemplateRepository {

    @Autowired
    SessionFactory sessionFactory;

    public TemplateRepository() {
    }

    public List<TaskTemplate> getAllTemplates() {
        List<TaskTemplate> allTemplates = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            allTemplates = session.createQuery("FROM TaskTemplate", TaskTemplate.class).list();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return allTemplates;
    }

    public void addOrEditTemplate(TaskTemplate template){
        org.hibernate.Transaction trx = null;
        try (Session session = sessionFactory.openSession()){
            trx = session.beginTransaction();
            session.saveOrUpdate(template);
            trx.commit();
        }
        catch (Exception e) {
            if (trx != null) { trx.rollback(); }
            e.printStackTrace();
        }
    }

    public TaskTemplate getTemplateById(int id){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            return session.get(TaskTemplate.class, id);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<TaskTemplate> getTemplatesByPeriod(int period) {
        List<TaskTemplate> periodTemplates = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            periodTemplates = session.createQuery("FROM TaskTemplate T WHERE T." + periodToColumnName(period) +"=1 AND T.active=1", TaskTemplate.class).list();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return periodTemplates;
    }

    private String periodToColumnName(int period){
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyyMM");
        YearMonth ym = YearMonth.parse(String.valueOf(period),df);
        String periodName = ym.getMonth().name().toLowerCase();
        return periodName.substring(0,3);
    }



}
