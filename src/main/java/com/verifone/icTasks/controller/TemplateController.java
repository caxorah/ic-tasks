package com.verifone.icTasks.controller;

import com.verifone.icTasks.entity.TaskTemplate;
import com.verifone.icTasks.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class TemplateController {

    @Autowired
    TemplateService service;

    @RequestMapping("/newtemplate")
    public String addNewTemplate(Model model){
        TaskTemplate taskTemplate = new TaskTemplate();
        taskTemplate.setActive(true);
        model.addAttribute("template", taskTemplate);
        return "templateform";
    }

    @RequestMapping(value = "/templates", method = RequestMethod.POST)
    public String saveTemplate(TaskTemplate template) {
        service.saveTemplate(template);
        return "redirect:/templates";
    }

    @RequestMapping("/templates")
    public String showMasterList(Model model){
        List<TaskTemplate> templates = service.getTemplates();
        model.addAttribute("templates", templates);
        return "tasktemplates";
    }

    @RequestMapping("/templates/{id}")
    public String showTemplateDetails(@PathVariable String id, Model model){
        TaskTemplate template = service.getTemplate(id);
        model.addAttribute("template",template);
        return "templateform";
    }




}

