package com.verifone.icTasks.service;

import org.springframework.context.ConfigurableApplicationContext;

public class CloseThread implements Runnable {

    ConfigurableApplicationContext ctx;

    public CloseThread(ConfigurableApplicationContext ctx) {
        this.ctx = ctx;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
            ctx.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

