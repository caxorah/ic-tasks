package com.verifone.icTasks.controller;

import com.verifone.icTasks.service.CloseThread;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ShutdownController implements ApplicationContextAware {

    private ApplicationContext context;

    @RequestMapping("/goodbye")
    public String shutdownContext(Model model) {

        ConfigurableApplicationContext ctx = (ConfigurableApplicationContext) context;
        new Thread(new CloseThread(ctx)).start();
        return "goodbye";
    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.context = ctx;

    }





}