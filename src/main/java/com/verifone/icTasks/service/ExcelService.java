package com.verifone.icTasks.service;

import com.verifone.icTasks.entity.Task;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.*;

public class ExcelService {

    final private String fileLocation = "\\\\wawwvfile1\\dept\\Intercompany Project\\AGIS Tracker.xlsx";
    private final Workbook workbook;

    public ExcelService() throws IOException {
        FileInputStream file = new FileInputStream(fileLocation);
        this.workbook = new XSSFWorkbook(file);
    }

    //look for AGIS batch in tracker file basing on period and description keyword
    //returns batch number (Integer) or null if nothing was found
    public Integer findBatchInTracker (Task task){
        Integer batchFoundNumber = null;
        Sheet trackerSheet = workbook.getSheet("Tracker");

        int batchNumColIndex = 2;

        for (Row row:trackerSheet){
            if (row!=null && dateCellIsNumeric(row)){
                if (descrAndPeriodEqual(task,row)) {
                    if (entityIsNumeric(task)) {
                        if (entityOutEqual(task,row)){
                            batchFoundNumber = (int) row.getCell(batchNumColIndex).getNumericCellValue();
                            break;
                        }
                    } else {
                        batchFoundNumber = (int) row.getCell(batchNumColIndex).getNumericCellValue();
                        break;
                    }
                }
            }
        }
        return batchFoundNumber;
    }

    private boolean dateCellIsNumeric(Row row){
        int dateColIndex = 5;
        Cell dateCell = row.getCell(dateColIndex);
        if (dateCell!=null){
            return dateCell.getCellType() == CellType.NUMERIC;
        } else {
            return false;
        }

    }

    private boolean descrAndPeriodEqual(Task task,Row row){
        int descrColIndex = 9;
        int dateColIndex = 5;

        String batchDescr = row.getCell(descrColIndex).getStringCellValue();
        YearMonth batchPeriod = YearMonth.from(row.getCell(dateColIndex).getLocalDateTimeCellValue());
        String taskDescr = task.getName();
        YearMonth taskPeriod = PeriodService.convertIntToYM(task.getPeriod());

        return batchPeriod.equals(taskPeriod) && batchDescr.contains(taskDescr);
    }

    private boolean entityIsNumeric(Task task){
        try {
            Integer.parseInt(task.getEntityOut());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean entityOutEqual(Task task, Row row){
        int entityOutColIndex = 0;
        int taskEntity = Integer.parseInt(task.getEntityOut());
        int batchEntity = (int) row.getCell(entityOutColIndex).getNumericCellValue();
        return taskEntity == batchEntity;
    }


}
