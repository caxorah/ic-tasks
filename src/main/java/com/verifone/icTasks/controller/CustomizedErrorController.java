package com.verifone.icTasks.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomizedErrorController implements ErrorController {

    @RequestMapping("/error")
    public String handleError(HttpServletRequest request, Model model){
        String errorMessage;
        int errCode = (int) request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
        Exception ex = (Exception) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
        errorMessage = errCode + " " + (ex==null?"":ex.getMessage());
        model.addAttribute("errorcode",errorMessage);
        return "errorpage";
    }


    @Override
    public String getErrorPath() {
        return null;
    }
}