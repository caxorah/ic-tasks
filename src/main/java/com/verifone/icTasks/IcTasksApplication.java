package com.verifone.icTasks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcTasksApplication {

	public static void main(String[] args) {

		try {
			SpringApplication.run(IcTasksApplication.class, args);
		} catch (Throwable t){
			//do nothing, it means app is running already
		}

		try {
			String url = "http://localhost:9099/home";
			Runtime rt = Runtime.getRuntime();
			rt.exec( "rundll32 url.dll,FileProtocolHandler " + url);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}


}
