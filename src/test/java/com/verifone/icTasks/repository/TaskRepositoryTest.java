package com.verifone.icTasks.repository;

import com.verifone.icTasks.service.PeriodService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TaskRepositoryTest {



    int getMinPeriod() {
        SessionFactory sf  = new org.hibernate.cfg.Configuration().configure().buildSessionFactory();
        int minPeriod = PeriodService.convertDateToPeriodInt(LocalDate.now().minusMonths(2));
        try(Session session = sf.openSession()){
            session.beginTransaction();
            minPeriod = (int) session.createQuery("SELECT MIN(period) FROM Task t").list().get(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return minPeriod;
    }


    int getMaxPeriod() {
        SessionFactory sf  = new org.hibernate.cfg.Configuration().configure().buildSessionFactory();
        int maxPeriod = PeriodService.convertDateToPeriodInt(LocalDate.now().minusMonths(2));
        try(Session session = sf.openSession()){
            session.beginTransaction();
            maxPeriod = (int) session.createQuery("SELECT MAX(period) FROM Task t").list().get(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return maxPeriod;
    }

    @Test
    void testListAllPeriods(){
        PeriodService.buildPeriodsListAllString(getMinPeriod(),getMaxPeriod());
    }
}