package com.verifone.icTasks.controller;

import com.verifone.icTasks.entity.TaskDao;
import com.verifone.icTasks.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    TaskService taskService;

    @Autowired
    List<String> allPeriods;

    @Autowired
    List<String> allStatus;

    @PostMapping("/generatetasks")
    public String createTasksForPeriod (@RequestParam("selectPeriod2") String selectPeriod2, HttpServletRequest request){
        taskService.createTasksForPeriod(selectPeriod2);
        request.getSession().setAttribute("sessionPeriod",selectPeriod2);
        return "redirect:/home";
    }

    @GetMapping("/loadtasks")
    public String getTasksForPeriod (@RequestParam ("selectPeriod") String selectPeriod, Model model, HttpServletRequest request){
        List<TaskDao> periodTasks = taskService.getPeriodTasks(selectPeriod);
        model.addAttribute("periodTasks",periodTasks);
        model.addAttribute("nowPeriod",selectPeriod);
        model.addAttribute("allPeriods",allPeriods);
        model.addAttribute("allStatus", allStatus);
        request.getSession().setAttribute("sessionPeriod",selectPeriod);
        return "periodtasks";
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.POST)
    public String saveChangesToTask(TaskDao taskDao){
        taskService.saveTask(taskDao);
        return "redirect:/home";
    }

    @RequestMapping("/tasks/{id}")
    public String showTaskDetails(@PathVariable String id, Model model){
        TaskDao taskDao = taskService.getTask(Integer.parseInt(id));
        model.addAttribute("taskDao",taskDao);
        model.addAttribute("allPeriods",allPeriods);
        model.addAttribute("allStatus",allStatus);
        return "taskform";
    }

    @RequestMapping("/tasks/delete/{id}")
    public String deleteTask (@PathVariable String id, Model model){
        taskService.deleteTaskById(Integer.parseInt(id));
        model.addAttribute("allPeriods",allPeriods);
        model.addAttribute("allStatus",allStatus);
        return "redirect:/home";
    }


    @RequestMapping("/tasks/batchupdate/{id}")
    public String updateBatchNumber(@PathVariable String id, Model model) throws IOException {
        int idInt = Integer.parseInt(id);
        TaskDao taskDao = taskService.getTask(idInt);
        taskService.assignRelatedBatchNumber(taskDao);
        TaskDao taskDaoAfterUpdate = taskService.getTask(idInt);
        model.addAttribute("taskDao",taskDaoAfterUpdate);
        model.addAttribute("allPeriods",allPeriods);
        model.addAttribute("allStatus",allStatus);
        return "redirect:/tasks/"+id;
    }



}
