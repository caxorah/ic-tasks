package com.verifone.icTasks.service;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class PeriodServiceTest {

    @Test
    void parseStringPeriodToInt() {

        //given
        String periodString = "JUN-21";
        int expected = 202106;

        //when
        int calculated = PeriodService.parseStringPeriodToInt(periodString);

        //then
        assertEquals(expected,calculated);

        //quick tests for other values
        calculated = PeriodService.parseStringPeriodToInt("JAN-20");
        assertEquals(202001,calculated);
        calculated = PeriodService.parseStringPeriodToInt("oct-19");
        assertEquals(201910,calculated);

    }

    @Test
    void convertStringToDate() {

        String period = "MAY-21";
        LocalDate expected = LocalDate.of(2021,5,1);

        LocalDate calculated = PeriodService.convertStringToDate(period);
        assertEquals(expected,calculated);

    }
}