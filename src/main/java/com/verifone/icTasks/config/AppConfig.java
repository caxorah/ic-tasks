package com.verifone.icTasks.config;

import com.verifone.icTasks.repository.TaskRepository;
import com.verifone.icTasks.service.PeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.HttpEncodingAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Arrays;
import java.util.List;

@Configuration
@Import({
        DispatcherServletAutoConfiguration.class,
        ErrorMvcAutoConfiguration.class,
        HttpEncodingAutoConfiguration.class,
        ThymeleafAutoConfiguration.class,
        WebMvcAutoConfiguration.class,
})
public class AppConfig {

    @Autowired
    TaskRepository taskRepository;

    @Bean
    public List<String> allPeriods(){
        return PeriodService.buildPeriodsListAllString(taskRepository.getMinPeriod(),taskRepository.getMaxPeriod());
    }

    @Bean
    public List<String> allStatus(){
        return Arrays.asList("not started", "in progress", "completed");
    }

}
