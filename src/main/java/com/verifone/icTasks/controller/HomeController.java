package com.verifone.icTasks.controller;

import com.verifone.icTasks.entity.TaskDao;
import com.verifone.icTasks.service.PeriodService;
import com.verifone.icTasks.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    TaskService taskService;

    @Autowired
    List<String> allPeriods;

    @Autowired
    List<String> allStatus;

    @RequestMapping("/home")
    public String displayHomepage (Model model, HttpServletRequest request){

        //nowPeriod - current one basing on now date or the one selected before
        String nowPeriod;
        if (request.getSession().getAttribute("sessionPeriod")==null) {
            nowPeriod = PeriodService.convertDateToPeriodString(LocalDate.now());
            request.getSession().setAttribute("sessionPeriod",nowPeriod);
        }
        else {
            nowPeriod = (String) request.getSession().getAttribute("sessionPeriod");
        }

        //task list for periodNow
        List<TaskDao> periodTasks;
        try {
            periodTasks = taskService.getPeriodTasks(nowPeriod);
        } catch (Exception e) {
            periodTasks = Collections.emptyList();
        }

        model.addAttribute("nowPeriod",nowPeriod);
        model.addAttribute("allPeriods",allPeriods);
        model.addAttribute("periodTasks",periodTasks);
        model.addAttribute("allStatus",allStatus);
        return "periodtasks";
    }


}
