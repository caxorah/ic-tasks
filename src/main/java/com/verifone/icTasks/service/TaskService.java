package com.verifone.icTasks.service;

import com.verifone.icTasks.entity.Task;
import com.verifone.icTasks.entity.TaskDao;
import com.verifone.icTasks.entity.TaskTemplate;
import com.verifone.icTasks.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

    @Autowired
    TaskRepository repository;

    @Autowired
    TemplateService templateService;

    public void saveTask(TaskDao taskDao){
        Task task = taskDao.convertToTask();
        repository.saveOrUpdateTask(task);
    }



    public void createTasksForPeriod(String period){
        int periodAsNum = PeriodService.parseStringPeriodToInt(period);
        List<TaskTemplate> templatesForPeriod = templateService.getTemplatesForPeriod(periodAsNum);
        templatesForPeriod.stream().map((t)->parseTemplateToTask(t,periodAsNum)).forEach(t->repository.saveOrUpdateTask(t));
    }

    private Task parseTemplateToTask(TaskTemplate template, int period){
        Task task = new Task();
        task.setEntityOut(template.getEntityOut());
        task.setEntityIn(template.getEntityIn());
        task.setCurrency(template.getCurrency());
        task.setName(template.getName());
        task.setDataProvider(template.getDataProvider());
        task.setDescription(template.getDescription());
        task.setTransactionType(template.getTransactionType());
        task.setTemplateId(template.getId());
        task.setPeriod(period);
        task.setStatus("not started");
        task.setComment("");
        task.setBatchNumber(null);  //i hope null will not display in html/thymeleaf
        return task;
    }


    public TaskDao getTask(int id) {
        return new TaskDao().fromTask(repository.getTaskById(id));
    }

    public void deleteTaskById(int id){
        Task task = repository.getTaskById(id);
        repository.deleteTask(task);
    }

    public List<TaskDao> getPeriodTasks(String periodString) {
        int periodAsNum = PeriodService.parseStringPeriodToInt(periodString);
        return repository.getTasksByPeriod(periodAsNum).stream().map(t->new TaskDao().fromTask(t)).collect(Collectors.toList());
    }


    public void assignRelatedBatchNumber(TaskDao taskDao) throws IOException {
        ExcelService excelService = new ExcelService();
        Integer foundBatchResult = excelService.findBatchInTracker(taskDao.convertToTask());
        taskDao.setBatchNumber(foundBatchResult);
        saveTask(taskDao);
    }


}
