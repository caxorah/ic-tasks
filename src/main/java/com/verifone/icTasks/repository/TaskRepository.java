package com.verifone.icTasks.repository;

import com.verifone.icTasks.entity.Task;
import com.verifone.icTasks.service.PeriodService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TaskRepository {

    @Autowired
    SessionFactory sessionFactory;

    public List<Task> getTasksByPeriod(int period){
        List<Task> periodTasks = new ArrayList<>();
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            periodTasks = session.createQuery("SELECT t FROM Task t WHERE t.period="+period, Task.class).list();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return periodTasks;
    }


    public void saveOrUpdateTask(Task task) {
        Transaction trx = null;
        try (Session session = sessionFactory.openSession()){
            trx = session.beginTransaction();
            session.saveOrUpdate(task);
            trx.commit();
        } catch (Exception e) {
            if (trx != null) { trx.rollback(); }
            e.printStackTrace();
        }
    }

    public Task getTaskById(int id){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            return session.get(Task.class, id);
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public void deleteTask(Task task){
        Transaction trx = null;
        try (Session session = sessionFactory.openSession()) {
            trx = session.beginTransaction();
            session.delete(task);
            trx.commit();
        } catch (Exception e) {
            if (trx != null) { trx.rollback(); }
            e.printStackTrace();
        }
    }

    public int getMinPeriod(){
        int minPeriod = PeriodService.convertDateToPeriodInt(LocalDate.now().minusMonths(2));
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            minPeriod = (int) session.createQuery("SELECT MIN(period) FROM Task t").list().get(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  minPeriod;
    }

    public int getMaxPeriod(){
        int maxPeriod = PeriodService.convertDateToPeriodInt(LocalDate.now().plusMonths(2));
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            maxPeriod = (int) session.createQuery("SELECT MAX(period) FROM Task t").list().get(0);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return  maxPeriod;
    }
}
